# Things

This repo is a collection of each day's Idos ("Eye Doos"):

* Things I Did Do
* Things I Didn't Do
* Things I Need To Do

## Things I Did Do

These are the core of ThingsI.do: Complete Idos (whether or not I set out to do them). e.g. Chores, projects, unexpected tasks, etc.

## Things I Didn't Do

These are incomplete Idos or Idos that went unstarted. e.g. Missed chores, project parts that ran out of time, etc.

## Things I Need To Do

These are ideas for a days Idos and may float from week to week, depending on availability of time.
